/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-23 17:14:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"
//DNS Demo
static void TestTask(void *param)
{
	T_OPENAT_NETWORK_STATUS status = {0};
	while (1)
	{
		//获取网络状态
		BOOL err = iot_network_get_status(&status);
		if (!err)
		{
			iot_debug_print("[socket] iot_network_get_status error");
		}
		else
		{
			iot_debug_print("[socket] state:%d,csq:%d,simpresent:%d", status.state, status.csq, status.simpresent);
			T_OPENAT_NETWORK_CONNECT networkparam = {0};
			switch (status.state)
			{
			case OPENAT_NETWORK_DISCONNECT: //网络断开 表示GPRS网络不可用澹，无法进行数据连接，有可能可以打电话
				iot_debug_print("[socket] OPENAT_NETWORK_DISCONNECT");
				break;
			case OPENAT_NETWORK_READY: //网络已连接 表示GPRS网络可用，可以进行链路激活
				memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
				//建立网络连接，实际为pdp激活流程
				iot_network_connect(&networkparam);
				iot_debug_print("[socket] OPENAT_NETWORK_READY");
				break;
			case OPENAT_NETWORK_LINKED: //链路已经激活 PDP已经激活，可以通过socket接口建立数据连接
				iot_debug_print("[socket] OPENAT_NETWORK_LINKED");
				char *name = "www.baidu.com";
				struct openat_hostent *hostentP = NULL;
				char *ipAddr = NULL;
				//获取域名对应的IP地址
				hostentP = gethostbyname(name);
				if (!hostentP)
				{
					iot_debug_print("[socket] gethostbyname %s fail", name);
					break;
				}
				// 将ip转换成字符串
				ipAddr = ipaddr_ntoa((const openat_ip_addr_t *)hostentP->h_addr_list[0]);
				iot_debug_print("[socket] gethostbyname %s ip %s", name, ipAddr);
				break;
			}
		}
		osiThreadSleep(2000);
	}
}
//main函数
int appimg_enter(void *param)
{
	//系统休眠
	iot_os_sleep(10000);

	//创建一个任务
	osiThreadCreate("TestTask", TestTask, NULL, OSI_PRIORITY_NORMAL, 2048, 0);

	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
