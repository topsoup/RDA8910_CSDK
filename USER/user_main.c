/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-31 18:58:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"

#include "http_client.h"

HANDLE TestTask_HANDLE = NULL;
uint8 NetWorkCbMessage = 0;
int socketfd = -1;

http_client_handle_t client_handle_t = {0};

void http_cb(http_client_event *evt)
{
	iot_os_sleep(1000);//这里需要一个延时，不然下载的过程中，打印速度太快，日志会丢失，不需要查看日志的话可以忽略
	iot_debug_print("[http_client] http_client_event_state:%d", evt->state);
	iot_debug_print("[http_client] respons_state:%s", evt->respons_state);
	iot_debug_print("[http_client] data:%s", evt->data);
	iot_debug_print("[http_client] datalen:%d", evt->datalen);
}
static void TestTask(void *param)
{
	bool NetLink = FALSE;
	while (NetLink == FALSE)
	{
		T_OPENAT_NETWORK_CONNECT networkparam = {0};
		switch (NetWorkCbMessage)
		{
		case OPENAT_NETWORK_DISCONNECT: //网络断开 表示GPRS网络不可用澹，无法进行数据连接，有可能可以打电话
			iot_debug_print("[socket] OPENAT_NETWORK_DISCONNECT");
			iot_os_sleep(10000);
			break;
		case OPENAT_NETWORK_READY: //网络已连接 表示GPRS网络可用，可以进行链路激活
			iot_debug_print("[socket] OPENAT_NETWORK_READY");
			memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
			//建立网络连接，实际为pdp激活流程
			iot_network_connect(&networkparam);
			iot_os_sleep(500);
			break;
		case OPENAT_NETWORK_LINKED: //链路已经激活 PDP已经激活，可以通过socket接口建立数据连接
			iot_debug_print("[socket] OPENAT_NETWORK_LINKED");
			NetLink = TRUE;
			break;
		}
	}
	if (NetLink == TRUE)
	{
		http_client_config_t config = {0};
		//////////////GET测试1/////////////////////////
		//config.url = "https://bkimg.cdn.bcebos.com/pic/37d3d539b6003af356370ff93f2ac65c1138b69e?x-bce-process=image/resize,m_lfit,w_268,limit_1/format,f_jpg";
		//config.method = HTTP_METHOD_GET;
		//////////////////////////////////////////////////
		
		//////////////GET测试2/////////////////////////
		//config.url = "https://i0.hdslb.com/bfs/album/8ac5f3eecccb17dfe8702eff13a4fb51d59cfc4b.jpg";
		//config.method = HTTP_METHOD_GET;
		//////////////////////////////////////////////////

		///////////////////POST测试////////////////////////
		config.url = "http://httpbin.org/post";
		config.table_data = "123456";
		config.method = HTTP_METHOD_POST;
		////////////////////////////////////
		config.event_handler = http_cb;
		//创建对象
		client_handle_t = http_client_new(&config);

		iot_debug_print("[http_client] init_state:%d", http_get_init_state(&client_handle_t));
		if (http_get_init_state(&client_handle_t) == HTTP_INIT_OK)
		{
			http_client_perform(&client_handle_t);
		}
	}
	iot_os_delete_task(TestTask_HANDLE);
}
static void NetWorkCb(E_OPENAT_NETWORK_STATE state)
{
	NetWorkCbMessage = state;
}
//main函数
int appimg_enter(void *param)
{
	//系统休眠
	iot_os_sleep(10000);
	//注册网络状态回调函数
	iot_network_set_cb(NetWorkCb);
	//创建一个任务
	//TestTask_HANDLE =
	TestTask_HANDLE = iot_os_create_task(TestTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "TestTask");
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
