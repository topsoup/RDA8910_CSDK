#ifndef  __IOT_VAT_H__
#define  __IOT_VAT_H__

#include "iot_network.h"

#define URC_QUEUE_COUNT 10                      /* global urc count */
#define AT_CMD_QUEUE_COUNT 50
#define AT_CMD_EXECUTE_DELAY  10                /* 10 ms */

#define AT_CMD_DELAY "DELAY:"
#define AT_CMD_END "\x0d\x0a"
#define AT_CMD_CR  '\x0d'
#define AT_CMD_LF  '\x0a'
#define STR_TO_INT(x) 	(x-'0') 					/*数字的char转换为int*/
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))

typedef enum
{
    AT_RSP_ERROR = -1,
    AT_RSP_WAIT= 0, 
    AT_RSP_CONTINUE = 1,                        /* 继续执行下一条AT队列里的命令 */
    AT_RSP_PAUSE= 2,                            /* 暂停执行AT队列命令 */
    AT_RSP_FINISH = 3,                          /* 停止执行AT队列命令 */

    AT_RSP_FUN_OVER = 4,                        /* 功能模块化AT命令组执行完毕，可以把本次运行的功能清除 */
    AT_RSP_STEP_MIN = 10,
    AT_RSP_STEP = 20,                           /* 继续执行本条AT命令 */
    AT_RSP_STEP_MAX = 30,

}AtCmdRsp;

typedef AtCmdRsp (*AtCmdRspCB)(u8 *pRspStr);
typedef VOID (*UrcCB)(u8 *pUrcStr, u16 len);
typedef VOID (*ResultNotifyCb)(BOOL result);

typedef struct AtCmdEntityTag
{
    u8* p_atCmdStr;
    u16 cmdLen;
    AtCmdRspCB p_atCmdCallBack;
}AtCmdEntity;

typedef struct UrcEntityTag
{
    u8* p_urcStr;
    UrcCB p_urcCallBack;

}UrcEntity;

typedef struct _CELL_INFO
{
	u32 CellId;  //cell ID
	u32 Lac;  //LAC
	u16 Mcc;  //MCC
	u16 Mnc;  //MNC
	u16 rssi; //rssi
}CELL_INFO;

typedef struct _gsmloc_cellinfo
{
	CELL_INFO Cellinfo[6];
}gsmloc_cellinfo;


VOID iot_vat_queue_init(VOID);
BOOL iot_vat_queue_is_empty(VOID);
BOOL iot_vat_queue_fun_set(u8 funCount);
BOOL iot_vat_queue_append(AtCmdEntity atCmdEntity);
BOOL iot_vat_queue_fun_append(AtCmdEntity atCmdEntity[],u8 funCount);
VOID iot_vat_SendCMD(VOID);
VOID iot_vat_Modeuleinit(VOID);

#endif

