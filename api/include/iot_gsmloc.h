#ifndef  __IOT_GSMLOC_H__
#define  __IOT_GSMLOC_H__


#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"
#include "iot_network.h"
#include "iot_socket.h"
#include "iot_vat.h"

#define GSMLOC_SERVER_UDP_IP "101.37.130.133"
#define GSMLOC_SERVER_UDP_PORT 2988 

typedef struct {
    UINT8 type;
    UINT8 data;
}DEMO_SOCKET_MESSAGE;

static HANDLE g_s_gsmloc_task;
static u8 GSMLOC_IMEI[16] = {0};
extern gsmloc_cellinfo GSMLOC_CELL;

void iot_gsmloc_init(void);
VOID iot_get_Gsmloc(u8* buf);



#endif

