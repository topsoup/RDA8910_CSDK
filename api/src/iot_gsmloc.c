#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"
#include "iot_network.h"
#include "iot_socket.h"
#include "iot_vat.h"
#include "iot_gsmloc.h"



#define GSMLOC_MSG_NETWORK_READY (0)
#define GSMLOC_MSG_NETWORK_LINKED (1)

#define gsmloc_dbg iot_debug_print

int gsmloc_reagyflg = 0;

static AtCmdRsp AtCmdCb_wimei(u8* pRspStr)
{
	iot_debug_print("[gsmloc]AtCmdCb_wimei");
    AtCmdRsp  rspValue = AT_RSP_WAIT;
    u8 *rspStrTable[ ] = {"+CME ERROR","+WIMEI:","OK"};
    s16  rspType = -1;
    u8* imei = GSMLOC_IMEI;
    u8  i = 0;
    u8  *p = pRspStr + 2;
    for (i = 0; i < sizeof(rspStrTable) / sizeof(rspStrTable[0]); i++)
    {
        if (!strncmp(rspStrTable[i], p, strlen(rspStrTable[i])))
        {
            rspType = i;
            if (rspType == 1){
				strncpy(imei,p+strlen(rspStrTable[i]),15);
				iot_debug_print("[vat]imei: %s",imei);
            }
            break;
        }
    }
    switch (rspType)
    {
        case 0:  /* ERROR */
        rspValue = AT_RSP_ERROR;
        break;

        case 1:  /* +wimei */
		rspValue  = AT_RSP_WAIT;
        break;

		case 2:  /* OK */
        rspValue  = AT_RSP_CONTINUE;
        break;

        default:
        break;
    }
    return rspValue;
}
static u8 state = 0xf;
static AtCmdRsp AtCmdCb_EEMGINFO(u8* pRspStr)
{
	iot_debug_print("[gsmloc]AtCmdCb_EEMGINFO");
    AtCmdRsp  rspValue = AT_RSP_WAIT;
    u8 *rspStrTable[ ] = {"\r\n+CME ERROR","+EEMGINFO: ", "\r\nOK"};
    s16  rspType = -1;
	u8 type = 0xf;
    u8  i = 0;
    u8  *p = pRspStr;
    for (i = 0; i < sizeof(rspStrTable) / sizeof(rspStrTable[0]); i++)
    {
        if (!strncmp(rspStrTable[i], p, strlen(rspStrTable[i])))
        {
            rspType = i;
            if (rspType == 1){
				
				state = STR_TO_INT(p[strlen(rspStrTable[i])]);
				type = STR_TO_INT(p[strlen(rspStrTable[i])+2]);
				iot_debug_print("[gsmloc]state: %d, type: %d",state, type);
            }
            break;
        }
    }
    switch (rspType)
    {
		case 0:  /* ERROR */
		rspValue = AT_RSP_ERROR;
		break;

		
		case 1:  /* +EEMLTESVC: */
			rspValue  = AT_RSP_WAIT;
        break;

		case 2:  /* OK */
		if( state == 3)
		{
			if(GSMLOC_CELL.Cellinfo[0].Mcc != 0)
			{
				gsmloc_reagyflg = 1;
				rspValue  = AT_RSP_CONTINUE;
			}
			else
				rspValue  = AT_RSP_WAIT;
		}
		else if(state == 0 || state == 1 || state == 2){
			rspValue  = AT_RSP_STEP;
		}
		break;

		default:
		break;
    }
    return rspValue;
}


static VOID gsmloc_SendATCmd(VOID)
{
	BOOL result = FALSE;
	AtCmdEntity atCmdInit[]={
		{"AT+WIMEI?"AT_CMD_END,11,AtCmdCb_wimei},
		{"AT+EEMOPT=1"AT_CMD_END,13,NULL},
		{AT_CMD_DELAY"1000",10,NULL},
		{"AT+EEMGINFO?"AT_CMD_END,14,AtCmdCb_EEMGINFO},
	};
	result = iot_vat_queue_fun_append(atCmdInit,sizeof(atCmdInit) / sizeof(atCmdInit[0]));
	iot_vat_SendCMD();
    return result;
}


static int iot_gsmloc_udp_connect_server(void)
{
    int socketfd;	
    // 创建udp socket
    socketfd = socket(OPENAT_AF_INET,OPENAT_SOCK_DGRAM,0);
    if (socketfd < 0)
    {
        gsmloc_dbg("[gsmloc] create udp socket error");
        return -1;
    }
    gsmloc_dbg("[gsmloc] create udp socket success");

    return socketfd;
}


static int iot_gsmloc_udp_send(int socketfd)
{
	gsmloc_dbg("[gsmloc] iot_gsmloc_udp_send");
    int send_len;
	int i = 0;
	struct openat_sockaddr_in udp_server_addr; 
	u8 gsmloc_data[256] = {0};
	u8* buf = gsmloc_data;
	buf += sprintf(buf, "%s,%u,", GSMLOC_IMEI, 3);
	for(i = 0; i < 6; i++)
	{
		if(GSMLOC_CELL.Cellinfo[i].Lac != 0 || GSMLOC_CELL.Cellinfo[i].CellId != 0)
			buf += sprintf(buf, "%u,%u,",GSMLOC_CELL.Cellinfo[i].Lac, GSMLOC_CELL.Cellinfo[i].CellId);
		else
			buf += sprintf(buf, ",,");
	}
	buf += sprintf(buf, "%u,%u", GSMLOC_CELL.Cellinfo[0].Mnc,GSMLOC_CELL.Cellinfo[0].Mcc);
	gsmloc_dbg("[gsmloc] gsmloc_data: %s", gsmloc_data);
	memset(&udp_server_addr, 0, sizeof(udp_server_addr)); // 初始化服务器地址  
    udp_server_addr.sin_family = OPENAT_AF_INET;  
    udp_server_addr.sin_port = htons((unsigned short)GSMLOC_SERVER_UDP_PORT);  
    inet_aton(GSMLOC_SERVER_UDP_IP,&udp_server_addr.sin_addr);

    // UDP 发送数据
    send_len = sendto(socketfd, gsmloc_data, strlen(gsmloc_data), 0,
    				  (struct sockaddr*)&udp_server_addr, sizeof(struct openat_sockaddr));
    gsmloc_dbg("[gsmloc] udp send result = %d", send_len);
    return send_len;
}

static int iot_gsmloc_udp_recv(int socketfd, u8* buf)
{
    unsigned char recv_buff[64] = {0};
    int recv_len;
	openat_socklen_t udp_server_len;
	
	struct openat_sockaddr_in udp_server_addr; 

	memset(&udp_server_addr, 0, sizeof(udp_server_addr)); // 初始化服务器地址  
    udp_server_addr.sin_family = OPENAT_AF_INET;  
    udp_server_addr.sin_port = htons((unsigned short)GSMLOC_SERVER_UDP_PORT);  
    inet_aton(GSMLOC_SERVER_UDP_IP,&udp_server_addr.sin_addr);
	udp_server_len = sizeof(udp_server_addr);

    // UDP 接受数据
    recv_len = recvfrom(socketfd, recv_buff, sizeof(recv_buff), 0, (struct sockaddr*)&udp_server_addr, &udp_server_len);
    gsmloc_dbg("[gsmloc] udp recv result %d data %s", recv_len, recv_buff);
	
    strncpy(buf, recv_buff, strlen(recv_buff));
    return recv_len;
}


VOID iot_get_Gsmloc(u8* buf)
{
    int socketfd, err, ret, count;

	u8 recv_buff[100] = {0};
	int i;

    socketfd = iot_gsmloc_udp_connect_server();

    if (socketfd >= 0)
    {
        while(1)
        {
            ret = iot_gsmloc_udp_send(socketfd);
            if(ret < 0)
            {
				err = socket_errno(socketfd);
				gsmloc_dbg("[gsmloc] send last error %d", err);
				if(err != OPENAT_EWOULDBLOCK)
				{
					break;
				}
				else
				{
					iot_os_sleep(200);
					continue;
				}
            }
			//阻塞读取
            ret = iot_gsmloc_udp_recv(socketfd, recv_buff);
            if(ret <= 0)
            {
                gsmloc_dbg("[gsmloc] recv error %d", socket_errno(socketfd));
                break;
            }
			if(++count >= 1)
			{
				gsmloc_dbg("[gsmloc] udp loop end");
				close(socketfd);
				break;
			}
        }
	    strncpy(buf, recv_buff+2, strlen(recv_buff+2)-3);
    }
}

static void iot_network_connetck(void)
{
    T_OPENAT_NETWORK_CONNECT networkparam;
    
    memset(&networkparam, 0, sizeof(T_OPENAT_NETWORK_CONNECT));
    memcpy(networkparam.apn, "CMNET", strlen("CMNET"));

    iot_network_connect(&networkparam);

}

static void iot_networkIndCallBack(E_OPENAT_NETWORK_STATE state)
{
    DEMO_SOCKET_MESSAGE* msgptr = iot_os_malloc(sizeof(DEMO_SOCKET_MESSAGE));
    gsmloc_dbg("[gsmloc] network ind state %d", state);
    if(state == OPENAT_NETWORK_LINKED)
    {
        msgptr->type = GSMLOC_MSG_NETWORK_LINKED;
        iot_os_send_message(g_s_gsmloc_task, (PVOID)msgptr);
        return;
    }
    else if(state == OPENAT_NETWORK_READY)
    {
        msgptr->type = GSMLOC_MSG_NETWORK_READY;
        iot_os_send_message(g_s_gsmloc_task,(PVOID)msgptr);
        return;
    }
    iot_os_free(msgptr);
}

static void iot_gsmloc_task(PVOID pParameter)
{
    DEMO_SOCKET_MESSAGE*    msg;

    gsmloc_dbg("[gsmloc] wait network ready....");

    while(1)
    {
        iot_os_wait_message(g_s_gsmloc_task, (PVOID)&msg);

        switch(msg->type)
        {
            case GSMLOC_MSG_NETWORK_READY:
                gsmloc_dbg("[gsmloc] network connecting....");
                iot_network_connetck();
                break;
            case GSMLOC_MSG_NETWORK_LINKED:
                gsmloc_dbg("[gsmloc] network connected");
				iot_os_sleep(1000);
				gsmloc_SendATCmd();
                break;
        }

        iot_os_free(msg);
    }
}

void iot_gsmloc_init(void)
{ 
    gsmloc_dbg("[gsmloc] iot_gsmloc_init");

    //注册网络状态回调函数
    iot_network_set_cb(iot_networkIndCallBack);

    g_s_gsmloc_task = iot_os_create_task(iot_gsmloc_task,
                        NULL,
                        4096,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "iot_gsmloc");
}
